<?php
return [
	'urlFormat' => 'path',
	'showScriptName' => false,
	'rules'=> [
		'post/<id:\d+>/<title:.*?>'=>'post/view',
		'posts/<tag:.*?>'=>'post/index',
		// REST patterns
		array('api/list', 'pattern'=>'api/<model:\w+>', 'verb'=>'GET'),
		array('api/view', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'GET'),
		array('api/update', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'PUT'),
		array('api/delete', 'pattern'=>'api/<model:\w+>/<id:\d+>', 'verb'=>'DELETE'),
		array('api/create', 'pattern'=>'api/<model:\w+>', 'verb'=>'POST'),
		'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
	],
];
<?php

return [
    'basePath'   => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'       => 'App',
    'preload'    => ['log'],

    'defaultController' => 'main',

    'aliases' => [
        'root' => dirname(__FILE__) . '/../../',
        'application' => 'root.application',
        'modules' => 'application.modules',
        'default' => 'modules.default',
        'views' => 'application.views',

        'ext' => 'application.extensions',
        'vendor' => 'root.vendor',
    ],
    'import'     => [
        'application.models.*',
        'application.components.*',

        'ext.mailer.*',

        'application.modules.default.controllers.*',
        'default.components.*',

        'application.modules.rights.*',
        'application.modules.rights.components.*',
    ],

    'controllerPath'    => PROJECT_ROOT . '/application/modules/default/controllers',
    'viewPath'          => PROJECT_ROOT . '/application/modules/default/views',
    'layoutPath'        => PROJECT_ROOT . '/application/modules/default/views',
    'modulePath'        => PROJECT_ROOT . '/application/modules',

    'modules'    => [
        'gii' => [
            'class'    => 'system.gii.GiiModule',
            'password' => '11111111',
        ],
        'user' => array(
            'class' => 'application.modules.user.UserModule',
            'modules' => array(
                'manage' => array(
                    'class' => 'user\modules\manage\ManageModule',
                ),
            ),
        ),

        'rights' => array(
            'install' => false,
            'superuserName' => 'Admin',
            'authenticatedName' => 'Authenticated',
            'userIdColumn' => 'id',
            'userNameColumn' => 'name',
            'enableBizRule' => true,
            'enableBizRuleData' => false,
            'displayDescription' => true,
            'flashSuccessKey' => 'RightsSuccess',
            'flashErrorKey' => 'RightsError',
            'baseUrl' => '/rights/assignment/view',
            'appLayout' => 'admin.views.layouts.page',
            'cssFile' => false,
        ),
    ],
    'components' => [
        'user'         => [
            //'class'          => 'RWebUser',
            'loginUrl'       => ['user/index/login'],
            'allowAutoLogin' => true,
        ],
        'urlManager'   => include 'url.manager.php',
        'db'           => [
            'connectionString' => 'mysql:host=87.76.91.14;dbname=dev',
            'emulatePrepare'   => true,
            'username'         => 'vlad',
            'password'         => '111111',
            'charset'          => 'utf8',

        ],
	   /* 'db'           => [
		    'connectionString' => 'mysql:host=localhost;dbname=dev',
		    'emulatePrepare'   => true,
		    'username'         => 'root',
		    'password'         => '',
		    'charset'          => 'utf8',

	    ],*/
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log'          => [
            'class'  => 'CLogRouter',
            'routes' => [
                [
                    'class'  => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                ],
            ],
        ],
        'request'      => [
            'class' => 'application.components.Request',
        ],
        'mailer' => [
            'class' => 'ext.mailer.YiiMail',
            'transportType' => 'php',
            'viewPath' => 'views.email',
            'logging' => true,
            'dryRun' => false,
        ],
    ],
    'params'     => [
        'adminEmail' => 'webmaster@example.com',
    ],
];
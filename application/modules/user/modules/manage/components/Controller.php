<?php
namespace user\modules\manage\components;

/**
 * Class Controller
 *
 * @package user\modules\manage\components
 */
class Controller extends \user\components\Controller
{
    public $layout = "//layouts/cabinet";

    public function filters()
    {
        return [
            'checkAuth',
            //'checkType - login',
        ];
    }

    /**
     * @param \CFilterChain $filterChain
     */
    public function filterCheckAuth(\CFilterChain $filterChain)
    {
        if (\Yii::app()->getUser()->isGuest) {
            $this->redirect('/');
        }

        $filterChain->run();
    }

}
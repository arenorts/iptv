<?php
namespace user\modules\manage\controllers;

use \user\modules\manage\components as Component;

class HistoryController extends Component\Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }


	public function actionPayment()
	{
		$this->render('payment');
	}

	public function actionRefill()
	{
		$this->render('refill');
	}

	public function actionReferral()
	{
		$this->render('referral');
	}

	public function actionDealerCharges()
	{
		$this->render('dealer_charges');
	}

	public function actionUserHistory()
	{
		$this->render('user_history');
	}



}
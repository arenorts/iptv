<?php
namespace user\modules\manage\controllers;

use user\models\User;
use \user\modules\manage\components as Component;

class SettingsController extends Component\Controller
{

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionPersonal()
    {
        /* @var User $user*/
        $user = User::model()->findByPk(\Yii::app()->user->id);

        if ($this->getRequest()->getIsPostRequest()) {
            $user->attributes = $this->getRequest()->getPost($user->formName());

            if ($user->save()) {
                \Yii::app()->getUser()->setName($user->getName());
            }

        }

        $this->render('personal', [
            'model' => $user
        ]);
    }

	public function actionSecurity()
	{
		$this->render('security');
	}

	public function actionSocial()
	{
		$this->render('social');
	}

	public function actionNotification()
	{
		$user = User::model()->findByPk(\Yii::app()->user->id);

		if ($this->getRequest()->getIsPostRequest()) {
			$user->attributes = $this->getRequest()->getPost($user->formName());

			if ($user->save()) {

			}

		}

		$this->render('notification', [
			'model' => $user
		]);
	}

}
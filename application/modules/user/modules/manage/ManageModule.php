<?php
namespace user\modules\manage;

/**
 * Class AdminModule
 *
 * @package user\modules\admin
 */
class ManageModule extends \WebModule
{
    protected function init()
    {
        parent::init();
        $this->controllerNamespace = 'user\modules\manage\controllers';
        $this->defaultController = 'index';
        //$this->setLayoutPath(\Yii::getPathOfAlias('admin.views.layouts'));
    }
}

<ul>
	<li><?= CHtml::link('История пополнений счета', 'refill')?></li>
	<li><?= CHtml::link('История оплат услуг', 'payment')?></li>
	<li><?= CHtml::link('История реферальных поступлений', 'referral')?></li>
	<li><?= CHtml::link('История дилерских начислений', 'dealerCharges')?></li>
	<li><?= CHtml::link('История действий клиента', 'userHistory')?></li>
</ul>

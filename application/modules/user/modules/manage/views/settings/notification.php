<ul>
	<li><?= CHtml::link('Личные данные', 'personal')?></li>
	<li><?= CHtml::link('Настройки безопасности', 'security')?></li>
	<li><?= CHtml::link('Настройки уведомлений', 'notification')?></li>
	<li><?= CHtml::link('Подключение соц. сетей', 'social')?></li>
</ul>

<?php

/**
 * @var \CActiveForm $form
 */

$form = $this->beginWidget('CActiveForm',
    [
        'id' => 'notification-form',
        //'enableAjaxValidation' => true,
        //'enableClientValidation' => false,
        //'clientOptions' => ['validateOnSubmit' => true, 'validateOnChange' => false],
        //'htmlOptions' => ['class' => 'form-horizontal box']
    ]
); ?>


<div class="row">
	<?php echo $form->labelEx($model,'email_fail_notification'); ?>
	<?php echo $form->checkBox($model, 'email_fail_notification' ); ?>
</div>

</br>

<div class="row">
	<?php echo $form->labelEx($model,'newsletter'); ?>
	<?php echo $form->checkBox($model, 'newsletter' ); ?>
</div>

</br>

<?= CHtml::submitButton('Отправить');  ?>

<?php $this->endWidget(); ?>
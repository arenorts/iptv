<ul>
	<li><?= CHtml::link('Личные данные', 'personal')?></li>
	<li><?= CHtml::link('Настройки безопасности', 'security')?></li>
	<li><?= CHtml::link('Настройки уведомлений', 'notification')?></li>
	<li><?= CHtml::link('Подключение соц. сетей', 'social')?></li>
</ul>

<?php

/**
 * @var \CActiveForm $form
 */

$form = $this->beginWidget('CActiveForm',
    [
        'id' => 'user-form',
        //'enableAjaxValidation' => true,
        //'enableClientValidation' => false,
        //'clientOptions' => ['validateOnSubmit' => true, 'validateOnChange' => false],
        //'htmlOptions' => ['class' => 'form-horizontal box']
    ]
); ?>


<?php echo $form->labelEx($model, 'name'); ?>
<?php echo $form->textField($model, 'name'); ?>
<?php echo $form->error($model, 'name'); ?>

</br>

<?php echo $form->labelEx($model, 'second_name'); ?>
<?php echo $form->textField($model, 'second_name'); ?>
<?php echo $form->error($model, 'second_name'); ?>

</br>


<div class="row">
<?php echo $form->labelEx($model,'gender'); ?>
<?php echo $form->radioButtonList($model, 'gender', ['male' => 'male', 'female' => 'female']) ?>
<?php echo $form->error($model,'gender'); ?>
</div>

</br>

<?php echo $form->labelEx($model, 'phone'); ?>
<?php echo $form->textField($model, 'phone'); ?>
<?php echo $form->error($model, 'phone'); ?>

</br>


<?= CHtml::submitButton('Сохранить');  ?>

<?php $this->endWidget(); ?>
<?php

class UpdateController extends Controller
{

	//curl -X PUT -d "username=newUserName&email=newEmail" http://yii/user/update
	public function actionIndex($id = null) {

		if (!$this->getRequest()->isPutRequest) {
			$this->_sendResponse(400, "Not put request");
		}

		if (!$id) {
			$this->_sendResponse(400, sprintf("Error: please select id of %s", $this->getModule()->id));
		}

		$restParams = $this->getRequest()->getRestParams();

		if (User::model()->updateByPk($id, $restParams)) {
			$this->_sendResponse(200, 'success');
		}

		$this->_sendResponse(500, "Error: Couldn't update user");

	}
}
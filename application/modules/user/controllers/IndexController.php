<?php

use \user\components as Component;

class IndexController extends Component\Controller
{

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {

            if (in_array($action->getId(), ['dashboard', 'order', 'history', 'settings'])) {
                $this->layout = 'views.layouts.cabinet';

                //$this->addBreadCrumb(\Yii::t('app', 'Профиль'), '/user/index/view');
            }

            return true;
        }

        return false;
    }



    public $breadcrumbs;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return \CMap::mergeArray(
            parent::filters(),
            [
                //'checkAuth - login, registration, logout',
                'checkType + dashboard, order',
            ]
        );
    }

    /**
     * @param \CFilterChain $filterChain
     */
    public function filterCheckAuth(\CFilterChain $filterChain)
    {
        if (!\Yii::app()->getUser()->isGuest) {
            $this->redirect(\Yii::app()->getUser()->returnUrl);
        }

        $filterChain->run();
    }

    /**
     * @param \CFilterChain $filterChain
     *
     * @throws \CHttpException
     */
    public function filterCheckType(\CFilterChain $filterChain)
    {
        if (\Yii::app()->getUser()->getState('type') !== \UserIdentity::TYPE_USER) {
            \Yii::app()->getUser()->logout();

            $this->redirect(['/site/index']);
        }

        $filterChain->run();
    }

    /**
     * @return string
     */
    public function allowedActions()
    {
        return 'login, registration, emailConfirm, recoverPassword, newPassword, logout, registerSuccess, success, emailSent, dashboard';
    }

    public function actionLogin()
    {
        $model = new LoginForm;

        if (isset($_POST['LoginForm'])) {

            $model->attributes=$_POST['LoginForm'];

            if ($model->validate() && $model->login()) {
                $this->redirect('/user/manage/dashboard');
            }

        }

        $this->render('login',array('model'=>$model));
    }

    public function actionRegistration()
    {
        $user = new \user\models\User(\user\models\User::SCENARIO_SIGN_UP);

        if ($this->getRequest()->hasParam($user->formName())) {
            $attributes = $this->getRequest()->getParam($user->formName());
            $user->setAttributes($attributes);

            $email = $user->email;
            $pass = $user->password;
            $user->access_code = \Randomness::randomStringPure(64);

            if ($user->save()) {
                $user->sendRegistrationSuccessMail();

                $identity = new UserIdentity($email, $pass);

                if ($identity->authenticate()) {
                    Yii::app()->user->login($identity);
                    $this->redirect('registerSuccess');
                }
            }

        }

        $user->password = null;

        $this->render('registration', [
                'model' => $user
            ]);
    }

    /**
     * @param $accessCode
     */
    public function actionEmailConfirm($accessCode)
    {
        /* @var \user\models\User $user */
        $user = \user\models\User::model()->findByAttributes(['access_code' => $accessCode]);

        if (!\Yii::app()->getUser()->isGuest) {
            \Yii::app()->getUser()->logout();
        }

        $user->activate(true);
        $user->autoLogin();

        if (!empty($user)) {
            $user->sendThankForRegistrationMail();
        }

        $this->render('email_confirm');
    }


    public function actionRecoverPassword()
    {
        $model = new \user\models\User(\user\models\User::SCENARIO_RECOVER_PASSWORD);

        if ($this->getRequest()->hasParam($model->formName())) {
            $model->attributes = $this->getRequest()->getParam($model->formName());

            /* @var \user\models\User $user */
            $user = $model->findByAttributes([
                'email' => $model->email
            ]);

            if ($user !== null) {
                $user->access_code = \Randomness::randomStringPure(64);

                if ($user->save()) {
                    $user->sendRecoverPasswordMail();
                    $this->redirect('emailSent');
                } else {
                    var_dump('user has not been saved');
                }
            }
        }

        $this->render(
            'recover',
            [
                'model' => isset($user) ? $user : $model,
            ]
        );
    }

    /**
     * @param string $accessCode
     */
    public function actionNewPassword($accessCode)
    {

        /* @var \user\models\User $user */
        $user = \user\models\User::model()->findByAttributes(['access_code' => $accessCode]);

        $user->setScenario(\user\models\User::SCENARIO_NEW_PASSWORD);

        if ($this->getRequest()->hasParam($user->formName())) {
            $user->attributes = $this->getRequest()->getParam($user->formName());
            $user->access_code = null;

            $password = $user->password;

            if ($user->save()) {
                $user->password = $password;
                //$user->authenticate();
                $this->redirect(['success']);
            }
        }
        $user->password = null;

        $this->render('new_password', ['model' => $user]);

    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionRegisterSuccess()
    {
        $this->render('register_success');
    }

    public function actionSuccess()
    {
        $this->render('success');
    }

    public function actionEmailSent()
    {
        $this->render('email_sent');
    }

    public function actionDashboard()
    {
        $this->render('dashboard');
    }

    public function actionOrder()
    {
        $this->h1 = 'Заказы';
        $this->render('order');
    }

    public function actionHistory()
    {
        $this->h1 = 'История';
        $this->render('history');
    }

    public function actionSettings()
    {
        $this->h1 = 'Настройки';
        $this->render('settings');
    }


}
<?php
use \user\models\User as User;

class GetController extends Controller
{

	public $defaultAction = 'index';

	public function actionIndex($id = null) {

		if ($id) {
			$userSet = User::model()->findByPk($id);
		} else {
			$userSet = User::model()->findAll();
		}

		if ($userSet) {
			$this->_sendResponse(200, $userSet);
		}

		$this->status = 'fail';
		$this->_sendResponse(404, 'No Item found with id '. $id);
	}

	/*
	 * example of scope, get all active|deleted users
	 * */
	public function actionOnlyActiveUsers() {
		$userSet = User::model()->active()->findAll();
		$this->_sendResponse(200, $userSet);
	}

	public function actionOnlyDeletedUsers() {
		$userSet = User::model()->deleted()->findAll();
		$this->_sendResponse(200, $userSet);
	}


	/*
	 * get related data, for ex. get all user`s log by user
	 * */

	public function actionGetUserLogs($id = null) {
		if (!$id) {
			$this->error = 'select user id';
			$this->_sendResponse(200);
		}

		$user = User::model()->findByPk($id);
		if (!$user) {
			$this->error = 'cant find user with id '. $id;
			$this->_sendResponse(200);
		}

		$this->_sendResponse(200, ['userLogs' => $user->userLogs]);
	}



}
<?php

class DeleteController extends Controller
{

	public function actionIndex($id = null) {

		/*if (!$this->getRequest()->isDeleteRequest) {
			$this->_sendResponse(400, "Not delete request");
		}*/

		if (!$id) {
			$this->_sendResponse(400, sprintf("Error: please select id of %s", $this->getModule()->id));
		}

		if (User::model()->deleteByPk($id)) {
			$this->_sendResponse(200, $num);
		} else {
			$this->_sendResponse(500,
				sprintf("Error: Couldn't delete model <b>%s</b> with ID <b>%s</b>.", $this->getModule()->id, $id));
		}

	}
}
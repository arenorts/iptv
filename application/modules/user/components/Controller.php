<?php
namespace user\components;

/**
 * Class Controller
 *
 * @package user\components
 */
class Controller extends \FrontEndController
{

    public function filters()
    {
        return \CMap::mergeArray(
            parent::filters(),
            [
                'checkAuth - login, registration, logout',
                //'checkType - login',
            ]
        );
    }

    /**
     * @param \CFilterChain $filterChain
     */
    public function filterCheckAuth(\CFilterChain $filterChain)
    {
        if (!\Yii::app()->getUser()->isGuest) {
            $this->redirect(\Yii::app()->getUser()->returnUrl);
        }

        $filterChain->run();
    }


    /**
     * @param \CFilterChain $filterChain
     *
     * @throws \CHttpException
     */
    public function filterCheckType(\CFilterChain $filterChain)
    {
        if (\Yii::app()->getUser()->getState('type') !== \UserIdentity::TYPE_USER) {
            \Yii::app()->getUser()->logout();

            $this->redirect('/');
        }

        $filterChain->run();
    }

}
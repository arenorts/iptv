<?php
namespace user\models;

use user\models\_base\BaseUser;

/**
 * Class User
 *
 * @package user\models
 */
class User extends BaseUser {

	const STATUS_ACTIVE = 0;
	const STATUS_DELETED = 9;

    const ACTIVE_FALSE = 0;
    const ACTIVE_TRUE = 1;

    const SCENARIO_SIGN_UP = 'signUp';
    const SCENARIO_SIGN_IN = 'signIn';
    const SCENARIO_RECOVER_PASSWORD = 'recoverPassword';
    const SCENARIO_NEW_PASSWORD = 'newPassword';
    const SCENARIO_CHANGE_PASSWORD = 'changePassword';

    const LOGIN_DURATION = 31536000;

    public $rulesAccepted;


    public function rules()
    {
        return [
            [
                'rulesAccepted',
                'required',
                'message'       => \Yii::t('advice', 'Необходимо принять условия сервиса'),
                'requiredValue' => '1',
                'on' => self::SCENARIO_SIGN_UP
            ],
            [
                'password, email',
                'required',
                'on' => [self::SCENARIO_SIGN_UP, self::SCENARIO_SIGN_IN, self::SCENARIO_NEW_PASSWORD],
                'message' => \Yii::t('app', 'Необходимо заполнить поле "{attribute}"')
            ],
            [
                'password',
                'filter',
                'filter' => [$this, 'hashPassword'],
                'on' => [self::SCENARIO_SIGN_UP, self::SCENARIO_NEW_PASSWORD, self::SCENARIO_CHANGE_PASSWORD]
            ],

            ['email', 'unique', 'on' => self::SCENARIO_SIGN_UP],

            ['password', 'length', 'max' => 60, 'min' => 6],
            ['email, password', 'required', 'on' => self::SCENARIO_SIGN_UP],
            ['email', 'required', 'on' => self::SCENARIO_RECOVER_PASSWORD],
	        ['type, status, active, email_confirmed, email_fail_notification, newsletter', 'numerical', 'integerOnly'=>true],
            ['name, email, password', 'length', 'max'=>128],
	        ['second_name', 'length', 'max'=>50],
	        ['phone', 'length', 'max'=>14],
            ['gender', 'length', 'max' => 6],
            ['birthday', 'safe'],
            ['id, type, name, second_name, gender, birthday, email, password, status, phone, email_fail_notification, newsletter', 'safe', 'on'=>'search'],
        ];
    }

    /**
     * @param string $password
     *
     * @return bool
     */
    public function validatePassword($password)
    {
        return crypt($password, $this->password) === $this->password;
    }

    /**
     * @param string $password
     *
     * @return string
     */
    public function hashPassword($password)
    {
        return crypt($password, \Randomness::blowfishSalt());
    }


    public function sendRegistrationSuccessMail()
    {
        $message = new \YiiMailMessage();
        $message->view = 'registration';
        $message->message->setSubject('Регистрация нового пользователя');
        $message->setBody(['user' => $this], 'text/html');
        $message->message->setTo($this->email);
        $message->message->setFrom('registration@example.com');

        \Yii::app()->mailer->send($message);
    }

    public function sendRecoverPasswordMail()
    {
        $message = new \YiiMailMessage();
        $message->view = 'recover';
        $message->message->setSubject('Восстановление пароля');
        $message->setBody(['user' => $this], 'text/html');
        $message->message->setTo($this->email);
        $message->message->setFrom('recovery@example.com');

        \Yii::app()->mailer->send($message);
    }

    public function sendThankForRegistrationMail()
    {
        $message = new \YiiMailMessage();
        $message->view = 'thank_for_registration';
        $message->message->setSubject('Спасибо за регистрацию на нашем сайте');
        $message->setBody(['user' => $this], 'text/html');
        $message->message->setTo($this->email);
        $message->message->setFrom('thank-for-registration@example.com');

        \Yii::app()->mailer->send($message);
    }

    public function activate($confirmEmail = false)
    {

        $this->active = self::ACTIVE_TRUE;
        $this->access_code = null;

        if ($confirmEmail) {
            $this->email_confirmed = 1;
        }

        if ($this->save(false)) {
            return true;
        }

        return false;
    }


    public function autoLogin()
    {
        $identity = $this->getIdentity();
        $identity->initAuth($this);

        /*if ($confirmEmail && !$this->isEmailConfirmed()) {
            $this->email_confirmed = self::EMAIL_CONFIRMED;
            $this->activate();
        }*/

        $this->login($identity);
    }

    /**
     * @param $identity
     */
    protected function login($identity)
    {
        \Yii::app()->user->login($identity, self::LOGIN_DURATION);
    }

    /**
     * @return \UserIdentity
     */
    protected function getIdentity()
    {
        return new \UserIdentity($this->email, $this->password);
    }

	public static function model($className = null) {
		return parent::model(get_called_class());
	}

	public function scopes() {
		return [
			'active' => [
				'condition' => $this->bindTableAlias(':alias.status = :userStatus'),
				'params' => ['userStatus' => self::STATUS_ACTIVE],
			],
			'deleted' => [
				'condition' => $this->bindTableAlias(':alias.status = :userStatus'),
				'params' => ['userStatus' => self::STATUS_DELETED],
			]
		];
	}

    /**
     * @return string
     */
    public function getName()
    {
        if ($this->name !== null) {
            return $this->name;
        } else {
            return mb_substr($this->email, 0, strpos($this->email, '@'));
        }
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'rulesAccepted' => 'Я согласен с правилами сервиса'
        ]);
    }

}
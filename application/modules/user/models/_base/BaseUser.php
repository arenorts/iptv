<?php

namespace user\models\_base;

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property integer $type
 * @property string $name
 * @property string $second_name
 * @property string $gender
 * @property string $birthday
 * @property string $email
 * @property string $password
 * @property integer $status
 * @property string $access_code
 * @property integer $active
 * @property integer $email_confirmed
 * @property string $phone
 * @property integer $email_fail_notification
 * @property integer $newsletter
 */
class BaseUser extends \ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type, status, active, email_confirmed, email_fail_notification, newsletter', 'numerical', 'integerOnly'=>true),
			array('name, email, password', 'length', 'max'=>128),
			array('second_name', 'length', 'max'=>50),
			array('gender', 'length', 'max'=>6),
			array('access_code', 'length', 'max'=>64),
			array('phone', 'length', 'max'=>14),
			array('birthday', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type, name, second_name, gender, birthday, email, password, status, access_code, active, email_confirmed, phone, email_fail_notification, newsletter', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'type' => 'Type',
			'name' => 'Name',
			'second_name' => 'Second Name',
			'gender' => 'Gender',
			'birthday' => 'Birthday',
			'email' => 'Email',
			'password' => 'Password',
			'status' => 'Status',
			'access_code' => 'Access Code',
            'active' => 'Active',
            'email_confirmed' => 'Email Confirmed',
			'phone' => 'Phone',
			'email_fail_notification' => 'Email Fail Notification',
			'newsletter' => 'Newsletter',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('second_name',$this->second_name,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('access_code',$this->access_code,true);
        $criteria->compare('active',$this->active);
        $criteria->compare('email_confirmed',$this->email_confirmed);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email_fail_notification',$this->email_fail_notification);
		$criteria->compare('newsletter',$this->newsletter);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

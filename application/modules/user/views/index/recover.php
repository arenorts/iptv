<?php
/* @var CActiveForm $form */
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Recover</h1>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm'); ?>

        <div class="row">
            <?php echo $form->labelEx($model,'email'); ?>
            <?php echo $form->textField($model,'email'); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton('Recover'); ?>
        </div>

    <?php $this->endWidget(); ?>

</div>

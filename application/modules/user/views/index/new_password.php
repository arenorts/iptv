<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Восстановление пароля</h1>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm'); ?>

        <div class="row">
            <?php echo $form->labelEx($model,'password'); ?>
            <?php echo $form->passwordField($model,'password'); ?>
            <?php echo $form->error($model,'password'); ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton('Изменить пароль'); ?>
        </div>

    <?php $this->endWidget(); ?>
</div>

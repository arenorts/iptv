<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>
	
	<div class="row">
		<?= $form->dropDownList($model, 'itemname', $itemnameSelectOptions); ?>
		<?= $form->error($model, 'itemname'); ?>
	</div>
	
	<div class="row buttons">
		<?= CHtml::submitButton(Rights::t('core', 'Assign')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
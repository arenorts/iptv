 <div class="flashes" style="position: absolute; height: 0">

	<?php if( Yii::app()->user->hasFlash('RightsSuccess')===true ):?>

	    <div class="flash success">

	        <?= Yii::app()->user->getFlash('RightsSuccess'); ?>

	    </div>

	<?php endif; ?>

	<?php if( Yii::app()->user->hasFlash('RightsError')===true ):?>

	    <div class="flash error">

	        <?= Yii::app()->user->getFlash('RightsError'); ?>

	    </div>

	<?php endif; ?>

 </div>
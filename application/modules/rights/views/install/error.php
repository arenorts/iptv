<div id="installer" class="error">

	<h2><?= Rights::t('install', 'Error'); ?></h2>

	<p class="red-text">
		<?= Rights::t('install', 'An error occurred while installing Rights.'); ?>
	</p>

    <p>
		<?= Rights::t('install', 'Please try again or consult the documentation.') ;?>
	</p>

</div>
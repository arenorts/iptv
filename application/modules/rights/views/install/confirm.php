<div id="installer" class="confirm">

	<h2><?= Rights::t('install', 'Install Rights'); ?></h2>

	<p class="red-text">
		<?= Rights::t('install', 'Rights is already installed!'); ?>
	</p>

	<p><?= Rights::t('install', 'Please confirm if you wish to reinstall.'); ?></p>

	<p>
		<?= CHtml::link(Rights::t('install', 'Yes'), array('install/run', 'confirm'=>1)); ?> /
		<?= CHtml::link(Rights::t('install', 'No'), Yii::app()->homeUrl); ?>
	</p>

	<p class="info"><?= Rights::t('install', 'Notice: All your existing data will be lost.'); ?></p>

</div>
<div id="installer" class="ready">

	<h2><?= Rights::t('install', 'Congratulations!'); ?></h2>

	<p class="green-text">
		<?= Rights::t('install', 'Rights has been installed succesfully.'); ?>
	</p>

	<p>
		<?= Rights::t('install', 'You can start by generating your authorization items') ;?>
		<?= CHtml::link(Rights::t('install', 'here'), array('authItem/generate')); ?>.
	</p>

</div>
<div class="form span-12 first">

<?php if( $model->scenario==='update' ): ?>

	<h3><?= Rights::getAuthItemTypeName($model->type); ?></h3>

<?php endif; ?>
	
<?php $form=$this->beginWidget('CActiveForm'); ?>

	<div class="row">
		<?= $form->labelEx($model, 'name'); ?>
		<?= $form->textField($model, 'name', array('maxlength'=>255, 'class'=>'text-field')); ?>
		<?= $form->error($model, 'name'); ?>
		<p class="hint"><?= Rights::t('core', 'Do not change the name unless you know what you are doing.'); ?></p>
	</div>

	<div class="row">
		<?= $form->labelEx($model, 'description'); ?>
		<?= $form->textField($model, 'description', array('maxlength'=>255, 'class'=>'text-field')); ?>
		<?= $form->error($model, 'description'); ?>
		<p class="hint"><?= Rights::t('core', 'A descriptive name for this item.'); ?></p>
	</div>

	<?php if( Rights::module()->enableBizRule===true ): ?>

		<div class="row">
			<?= $form->labelEx($model, 'bizRule'); ?>
			<?= $form->textField($model, 'bizRule', array('maxlength'=>255, 'class'=>'text-field')); ?>
			<?= $form->error($model, 'bizRule'); ?>
			<p class="hint"><?= Rights::t('core', 'Code that will be executed when performing access checking.'); ?></p>
		</div>

	<?php endif; ?>

	<?php if( Rights::module()->enableBizRule===true && Rights::module()->enableBizRuleData ): ?>

		<div class="row">
			<?= $form->labelEx($model, 'data'); ?>
			<?= $form->textField($model, 'data', array('maxlength'=>255, 'class'=>'text-field')); ?>
			<?= $form->error($model, 'data'); ?>
			<p class="hint"><?= Rights::t('core', 'Additional data available when executing the business rule.'); ?></p>
		</div>

	<?php endif; ?>

	<div class="row buttons">
		<?= CHtml::submitButton(Rights::t('core', 'Save')); ?> | <?= CHtml::link(Rights::t('core', 'Cancel'), Yii::app()->user->rightsReturnUrl); ?>
	</div>

<?php $this->endWidget(); ?>

</div>
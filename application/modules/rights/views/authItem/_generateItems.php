<?php if ($items['controllers'] !== array()): ?>

	<?php foreach ($items['controllers'] as $key => $item): ?>

		<?php if (isset($item['actions']) === true && $item['actions'] !== array()): ?>

			<?php $controllerKey = isset($moduleName) === true ? ucfirst($moduleName) . '.' . $item['name'] : $item['name']; ?>
			<?php $controllerExists = isset($existingItems[$controllerKey . '.*']); ?>

			<tr class="controller-row <?= $controllerExists === true ? 'exists' : ''; ?>">
				<td class="checkbox-column"><?= $controllerExists === false ? $form->checkBox($model, 'items[' . $controllerKey . '.*]') : ''; ?></td>
				<td class="name-column"><?= ucfirst($item['name']) . '.*'; ?></td>
				<td class="path-column"><?= substr($item['path'], $basePathLength + 1); ?></td>
			</tr>

			<?php $i = 0;
			foreach ($item['actions'] as $action): ?>

				<?php $actionKey = $controllerKey . '.' . ucfirst($action['name']); ?>
				<?php $actionExists = isset($existingItems[$actionKey]); ?>

				<tr class="action-row<?= $actionExists === true ? ' exists' : ''; ?><?= ($i++ % 2) === 0 ? ' odd' : ' even'; ?>">
					<td class="checkbox-column"><?= $actionExists === false ? $form->checkBox($model, 'items[' . $actionKey . ']') : ''; ?></td>
					<td class="name-column"><?= $action['name']; ?></td>
					<td class="path-column"><?= substr($item['path'], $basePathLength + 1) . (isset($action['line']) === true ? ':' . $action['line'] : ''); ?></td>
				</tr>

			<?php endforeach; ?>

		<?php endif; ?>

	<?php endforeach; ?>

<?php else: ?>

	<tr>
		<th class="no-items-row" colspan="3"><?= Rights::t('core', 'No actions found.'); ?></th>
	</tr>

<?php endif; ?>

<?php if ($items['modules'] !== array()): ?>

	<?php if ($displayModuleHeadingRow === true): ?>

		<tr>
			<th class="module-heading-row" colspan="3"><?= Rights::t('core', 'Modules'); ?></th>
		</tr>

	<?php endif; ?>

	<?php foreach ($items['modules'] as $moduleN => $moduleItems): ?>

		<tr>
			<th class="module-row" colspan="3"><?= (isset($moduleName) ? ucfirst($moduleName) .   'Module / ' . ucfirst($moduleN) : ucfirst($moduleN)) . 'Module'; ?></th>
		</tr>

		<?php $this->renderPartial('_generateItems', [
			'model' => $model,
			'form' => $form,
			'items' => $moduleItems,
			'existingItems' => $existingItems,
			'moduleName' => (isset($moduleName) ? $moduleName . '.' . ucfirst($moduleN) : ucfirst($moduleN)),
			'displayModuleHeadingRow' => false,
			'basePathLength' => $basePathLength,
		]); ?>

	<?php endforeach; ?>

<?php endif; ?>
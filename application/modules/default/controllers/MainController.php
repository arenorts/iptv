<?php

/**
 * Class MainController
 * Date: 18.09.13
 * Time: 14:32
 *
 */
class MainController extends DefaultController
{
    public function actionIndex()
    {
        $this->render('index');
    }
}
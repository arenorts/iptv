<?php $this->pageTitle = Yii::app()->name; ?>

<div id="st">
    <?php if (Yii::app()->getUser()->isGuest) { ?>
        <?= 'Привет, гость' ?>
    <?php } else { ?>
        <?= 'Привет, ' . Yii::app()->user->name ?>
    <?php } ?>
</div>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="language" content="en"/>

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection"/>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print"/>
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection"/>
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"/>

        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div class="container" id="page">

            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
            </div>

            <div id="mainmenu">
                <?php $this->widget(
                    'zii.widgets.CMenu',
                    [
                        'items' => [
                            ['label' => 'Заказы', 'url' => ['/user/manage/order']],
                            ['label' => 'История', 'url' => ['/user/manage/history']],
                            ['label' => 'Настройки', 'url' => ['/user/manage/settings']],
                            ['label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => ['/user/index/logout']]
                        ],
                    ]
                ); ?>
            </div>

            <h1><?= $this->h1 ?></h1>


            <?php echo $content; ?>

            <div id="footer">
                <?php echo Yii::powered(); ?>
            </div>

        </div>

    </body>
</html>










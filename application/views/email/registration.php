Успешно создан новый акк [<?= $user->name ?>]

Для подтверждения перейдите по ссылке

<?php echo CHtml::link(\Yii::t('app', 'Подтвердить адрес'),
                       Yii::app()->createAbsoluteUrl('/user/index/emailConfirm',
                                                  ['accessCode' => $user->access_code]), [
                           'style' => 'text-decoration: none; display: inline-block; width: 260px; padding: 15px 0; color: #fff; font-size: 20px; background-color: #e74b3b; font-weight: normal; border-radius: 5px;',
                           'title' => "Подтвердить"
                       ]);?>
<?php


use user\models\User;

class UserIdentity extends CUserIdentity
{
    const TYPE_USER = 'user';

    protected $id;
    protected $user;

    public function authenticate()
    {
        $user = $this->getUser();

        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else {
            if ($user->validatePassword($this->password)) {
                $this->id = $user->id;
                $this->initStates($user);
                $this->errorCode = self::ERROR_NONE;
            } else {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
        }

        return !$this->errorCode;
    }

    public function initAuth($user)
    {
        $this->id = $user->id;
        $this->initStates($user);
    }

    /**
     * @return User
     */
    public function getUser() {

        if ($this->user === null){
            $this->user = User::model()->findByAttributes(['email' => $this->username]);
        }

        return $this->user;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @param User $user
     */
    protected function initStates(User $user) {
        /*$this->setState('isActive', $user->isActive());
        $this->setState('isEmailConfirmed', $user->isEmailConfirmed());
        $this->setState('register_type', $user->register_type);*/

        $this->setState('email', $user->email);
        $this->setState('name', $user->name);
        $this->setState('type', self::TYPE_USER);

        /*$this->setState('second_name', $user->second_name);
        $this->setState('last_name', $user->last_name);
        $this->setState('phone', $user->phone);*/
    }
}
<?php

/**
 * Class ActiveRecord
 */
abstract class ActiveRecord extends CActiveRecord {
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_INSERT = 'insert';
	const SCENARIO_SEARCH = 'search';

	private $h;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @return static the static model class
	 */
	public static function model($className = null) {
		return parent::model(get_called_class());
	}

	/**
	 * @param CDbCriteria $criteria
	 *
	 * @return CDbCriteria
	 */
	public static function getCountCriteria(CDbCriteria $criteria) {
		$countCriteria = new \CDbCriteria();
		$countCriteria->mergeWith($criteria);
		foreach ($countCriteria->with as &$relation) {
			if (isset($relation['together']) && $relation['together'] === false) {
				$relation = null;
			}
		}

		return $countCriteria;
	}

	/**
	 * @return bool
	 */
	protected function beforeSave() {
		if (parent::beforeSave()) {
			foreach ($this->getMetaData()->columns as $name => $column) {
				if ($this->hasAttribute($name) && $this->$name === '') {
					$this->$name = null;
				}
			}

			return true;
		}

		return false;
	}

	protected function afterFind() {
		parent::afterFind();

		$this->h = $this->getAttributes();
	}

	/**
	 * @param string $attribute
	 *
	 * @return bool
	 */
	public function isDirty($attribute) {
		return empty($this->h[$attribute]) || $this->h[$attribute] != $this->getAttribute($attribute);
	}

	/**
	 * @param string $query
	 * @param string $bind
	 * @param bool $checkScope
	 *
	 * @return string
	 */
	public function bindTableAlias($query, $bind = ':alias', $checkScope = true) {
		return str_replace($bind, $this->getTableAlias(true, $checkScope), $query);
	}

	/**
	 * @return string
	 */
	public function formName() {
		return CHtml::modelName($this);
	}
}
<?php

class Request extends \CHttpRequest {
	/**
	 * @param string $key
	 *
	 * @return bool
	 */
	public function hasParam($key) {
		return $this->getParam($key) !== null;
	}

	/**
	 * @param string $key
	 *
	 * @return bool
	 */
	public function hasQuery($key) {
		return $this->getQuery($key) !== null;
	}

	/**
	 * @param string $key
	 *
	 * @return bool
	 */
	public function hasPost($key) {
		return $this->getPost($key) !== null;
	}

	public function getSafeParams(){
		$data = $_GET;
		array_walk($data, function($value, $key){
			$data[$key] = GxHtml::decode($value);
		});
		return $data;
	}
}
<?php

abstract class CustController extends CController {


    public function environment()
    {
        return isset($_SERVER['APPLICATION_ENV']) ? $_SERVER['APPLICATION_ENV'] : null;
    }

    public $menu = [];
    public $h1;


    public $breadcrumbs = [];
    public $assetsUrl;

    protected $uploadAssetsUrl = [];


    /**
     * @return CHttpRequest
     */
    public function getRequest() {
        return Yii::app()->getRequest();
    }

}

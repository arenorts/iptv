<?php

class m140525_114526_test_user_data extends CDbMigration
{
	/*
	 * insert 2 users
	 * + insert 3 logs
	 * */
	public function up() {
		$this->execute("INSERT INTO `aloha`.`user` (`name`, `type`, `surname`, `gender`, `birthday`, `email`, `password`, `status`)
		VALUES ('name ', 1, 'surname', 'female', '2014-05-25', 'asd@mail.ri', '123', 0);");

		$this->execute("INSERT INTO `aloha`.`user` (`name`, `type`, `surname`, `gender`, `birthday`, `email`, `password`, `status`)
		VALUES ('name ', 1, 'surname', 'female', '2014-05-25', 'asd@mail.ri', '123', 9);");


		$this->execute("INSERT INTO `aloha`.`user_log` (`user_id`) VALUES (1);");
		$this->execute("INSERT INTO `aloha`.`user_log` (`user_id`, `status`, `date`, `message`) VALUES (1, 9, '2014-05-25 15:11:40', 'testMessage');");
		$this->execute("INSERT INTO `aloha`.`user_log` (`user_id`, `status`, `date`, `message`) VALUES (2, 9, '2014-05-25 15:11:40', 'testMessage');");
	}

	public function down() {
		return true; // do nothing
	}
}
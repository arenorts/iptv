<?php

class m140525_085955_user extends CDbMigration
{
	public function up() {
		$this->execute("CREATE TABLE `user` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`type` TINYINT(1) UNSIGNED NULL DEFAULT NULL,
	`name` VARCHAR(128) NULL DEFAULT NULL,
	`surname` VARCHAR(128) NULL DEFAULT NULL,
	`gender` ENUM('female','male') NULL DEFAULT NULL,
	`birthday` DATE NULL DEFAULT NULL,
	`email` VARCHAR(128) NULL DEFAULT NULL,
	`password` VARCHAR(128) NULL DEFAULT NULL,
	`status` TINYINT(2) NULL DEFAULT NULL,
	PRIMARY KEY (`id`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
");
	}

	public function down() {
		$this->dropTable('user');
	}
}
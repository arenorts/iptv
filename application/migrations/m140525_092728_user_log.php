<?php

class m140525_092728_user_log extends CDbMigration
{
	public function up() {
		$this->execute("CREATE TABLE `user_log` (
	`id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
	`user_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',
	`status` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0',
	`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`message` VARCHAR(256) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `FK_user_log_user` (`user_id`),
	CONSTRAINT `FK_user_log_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
");
	}

	public function down() {
		$this->dropTable('user_log');
	}

}
<?php
mb_internal_encoding('UTF-8');
$webRoot = dirname(__FILE__);
define('PROJECT_ROOT', $webRoot . '/');

$config=dirname(__FILE__).'/application/config/main.php';

defined('YII_DEBUG') or define('YII_DEBUG',true);
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once(PROJECT_ROOT . '/vendor/yiisoft/yii/framework/YiiBase.php');

/**
 * Class IDECodeAssistHelper_CWebApplication
 *
 * @property RWebUser $user
 * @method RWebUser getUser()
 *
 * @property YiiMail $mailer
 *
 * @property Request $request
 * @method \Request getRequest()
 */
class IDECodeAssistHelper_CWebApplication extends CWebApplication { }


/**
 * Class Yii
 * @method static IDECodeAssistHelper_CWebApplication app()
 */

class Yii extends YiiBase { }

require_once(PROJECT_ROOT . '/vendor/autoload.php');
Yii::createWebApplication($config)->run();
